var Pubsub = require('node-redis-pubsub');
var co = require('co');

var wrap = require('..');
var co = require('co');

var conf = {};
var rq = new Pubsub(conf);
var ps = wrap(rq);


describe('Co Redis Pubsub', function () {

  it('Should works with co wrap', function (done) {
    co(function *(){
      var pubMsg = yield ps.emit('a test', { first: 'First message', second: 'Second message' });
      var subMsg = yield ps.on('a test');

      console.log('pub >>>>>>>>>', pubMsg);
      console.log('sub >>>>>>>>>', subMsg);
    }).then(done);
  });

  it('Should send and receive standard messages correctly', function (done) {
    rq.on('a test', function (data, channel) {
      console.log('data >>>>>>>', data);
      console.log('channel >>>>>', channel);
      data.first.should.equal('First message');
      data.second.should.equal('Second message');
      //channel.should.equal("onescope:a test");
      done();
    }, function () {
      rq.emit('a test', { first: 'First message', second: 'Second message' });
    });
  });
});

