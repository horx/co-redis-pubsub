'use strict';
let thunkify = require('thunkify');
let methods = ['on', 'off', 'subscribe', 'unsubscribe'];

module.exports = function(col) {
  methods.forEach(function(method){
      col[method] = thunkify(col[method]);
    });
  return col;
};
